# Curative Care

An app which can display costs of medical procedures of US hospitals.
This app will display nearby hospitals and the user can filter the prices accordingly. App
downloads nearby hospitals CDM from Github Repository and save it to local storage of phone
in SQL database. This App can work offline and will update data daily.

## Screenshots

This project is a starting point for a Flutter application.


<img src="https://gitlab.com/Darshpreet2000/curative-care-app/-/raw/master/screenshots/home_screen.png">


<img src="https://gitlab.com/Darshpreet2000/curative-care-app/-/raw/master/screenshots/search_screen.png">


